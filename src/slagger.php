<?php

return [
    /*
     *  swagger json 生成位置
     */
    'file' => public_path('swagger.json'),

    /*
     *  想要生成文档的 resource
     */
    'resources' => [],

    /*
     * 默认 json 模板
     */
    'default' => [
        'swagger' => '2.0',
        'info' => [
            'title' => '',
            'version' => '',
            'description' => '',
        ],
        'host' => '',
        'basePath' => '/api',
        'tags' => [],
        'schemes' => ['http', 'https'],
        'paths' => [],
        'securityDefinitions' => [
            'api' => [
                'type' => 'apiKey',
                'in' => 'header',
                'name' => 'Authorization',
            ]
        ],
        'definitions' => [],
    ],

    // swagger 部分 object 的模板
    'template' => [
        'path' => [
            'summary' => '',
            'operationId' => '',
            'tags' => '',
            'consumes' => ['application/json'],
            'produces' => ['application/json'],
            'parameters' => [],
            'responses' => [],
            'security' => [
                [
                    'api' => []
                ]
            ],
        ]
    ]
];
