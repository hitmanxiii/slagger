<?php
/**
 * User: hitman
 * Date: 12/10/2017
 * Time: 4:42 PM
 */

if(env('APP_ENV') == 'local') {
    Route::get('swagger-api', function(){
        return view('swagger', ['url' => url('/swagger.json')]);
    });
}

