<?php

namespace Hitman\Slagger;

use Hitman\Slagger\Commands\SlaggerAddPathCommand;
use Hitman\Slagger\Commands\SlaggerCommand;
use Hitman\Slagger\Commands\SlaggerGenDefinitionCommand;
use Illuminate\Support\ServiceProvider;

class SlaggerServiceProvider extends ServiceProvider
{
    public function boot()
    {
        // 后台模板
        $this->publishes([ __DIR__ . "/swagger.blade.php" => resource_path('views/swagger.blade.php') ], 'slagger');
        $this->publishes([__DIR__ . "/assets" => public_path('swagger')], 'slagger');
        $this->publishes([__DIR__ . '/slagger.php' => config_path('slagger.php')]);

        $this->loadRoutesFrom(__DIR__ . '/route.php');

        $this->commands([
            SlaggerCommand::class,
            SlaggerAddPathCommand::class,
            SlaggerGenDefinitionCommand::class
        ]);
    }
    
    public function register()
    {

    }
}
