<?php

namespace Hitman\Slagger\Commands;

use Illuminate\Console\Command;

class SlaggerCommand extends Command
{
    use CommandTrait;
    protected $signature = 'slagger:gen';

    protected $description = '创建 slagger json 数据';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        // 获取现有 api.json
        $config = $this->getSlaggerConfig();
        // 获取配置resources
        $resources = config('slagger.resources', []);
        $processedResource = collect($config['tags'])->pluck('name')->toArray();
        $addedResource = array_diff($resources, $processedResource);
        $config['tags'] = collect($resources)->map(function($r){
            return [
                'name' => $r
            ];
        })->toArray();
        // 根据新增的 resource 添加 path
        foreach($addedResource as $newResource){
            $paths = $this->genResourcePaths($newResource);
            $config['paths'] = array_merge($paths, $config['paths']);
        }
        // 保存 api.json
        $this->writeSlaggerConfig($config);
    }

    private function genIndexPath($resource){
        $path = $this->getPathTemplate($resource);

        $path['summary'] = "$resource 列表";
        $path['operationId'] = "{$resource}List";

        return $path;
    }
    private function genStorePath($resource){
        $path = $this->getPathTemplate($resource);

        $path['summary'] = "创建$resource";
        $path['operationId'] = "{$resource}Create";

        return $path;
    }
    private function genShowPath($resource){
        $path = $this->getPathTemplate($resource);

        $path['summary'] = "{$resource}详情";
        $path['operationId'] = "{$resource}Show";
        $path['parameters'] = [
            [
                'name' => 'id',
                'in' => 'path',
                'required' => true,
                'type' => 'integer',
            ]
        ];

        return $path;
    }
    private function genUpdatePath($resource){
        $path = $this->getPathTemplate($resource);

        $path['summary'] = "更新{$resource}";
        $path['operationId'] = "{$resource}Update";
        $path['parameters'] = [
            [
                'name' => 'id',
                'in' => 'path',
                'required' => true,
                'type' => 'integer',
            ]
        ];

        return $path;
    }
    private function genDeletePath($resource){
        $path = $this->getPathTemplate($resource);

        $path['summary'] = "删除{$resource}";
        $path['operationId'] = "{$resource}Delete";
        $path['parameters'] = [
            [
                'name' => 'id',
                'in' => 'path',
                'required' => true,
                'type' => 'integer',
            ]
        ];

        return $path;
    }

    private function genResourcePaths($resource)
    {
        return [
            "/{$resource}" => [
                "get" => $this->genIndexPath($resource),
                "post" => $this->genStorePath($resource),
            ],
            "/{$resource}/{id}" => [
                "get" => $this->genShowPath($resource),
                "put" => $this->genUpdatePath($resource),
                "delete" => $this->genDeletePath($resource),
            ]
        ];
    }
}
