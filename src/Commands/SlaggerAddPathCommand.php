<?php

namespace Hitman\Slagger\Commands;

use Illuminate\Console\Command;
use Illuminate\Pagination\Paginator;

class SlaggerAddPathCommand extends Command
{
    use CommandTrait;
    protected $signature = 'slagger:add';
    protected $description = '添加 path';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $type = $this->choice("选择生成类型？", ['普通请求', '资源请求'], 0);

        if($type == '普通请求'){
            $this->addNormal();
        } else {
            $this->addResource();
        }
    }

    public function addNormal()
    {
        $method = $this->choice('请求方法', ['get', 'post', 'put', 'delete'], 0);
        $path   = $this->ask('请求路径');
        $tag    = $this->ask('所属标签');
        $summary = $this->ask('接口描述');
        $parameters = [];

        while(1){
            $parameter = $this->ask('参数(按E结束)');
            if($parameter == "E") break;
            $data['name'] = $parameter;
            $data['in'] = $this->ask('参数位置(path,query)');
            $parameters[] = $data;
        }
        // 获取现有 api.json
        $this->addPath($method, $path, $tag, $summary, $parameters);
    }

    public function addResource()
    {
        $resourceName = $this->ask('Model名称');
        $resource = "App\\Models\\" . $resourceName;
        $model = new $resource;
        $table = $model->getTable();
        $fields = $this->getFields($table);
        $tag = $resourceName;
        $path = "/" . str_plural(strtolower($resourceName));

        if($this->confirm("生成列表请求?")){
            $this->genIndex($resourceName, $path, $tag);
        }

        if($this->confirm("生成保存请求?")){
            $this->genStore($resourceName, $path, $tag, $fields);
        }

        if($this->confirm("生成更新请求?")){
            $this->genUpdate($resourceName, $path, $tag, $fields);
        }

        if($this->confirm("生成详情请求?")){
            $this->genShow($resourceName, $path, $tag);
        }

        if($this->confirm("生成删除请求?")){
            $this->genDelete($resourceName, $path, $tag);
        }
    }

    private function genIndex($resource, $path, $tag)
    {
        $method = 'get';
        $summary = "{$resource} 列表";
        $parameters = [];

        $this->addPath($method, $path, $tag, $summary, $parameters);
    }

    private function genShow($resource, $path, $tag)
    {
        $method = 'get';
        $summary = "{$resource} 详情";
        $parameters = [
            [
                'name' => 'id',
                'type' => 'integer',
                'in' => 'path'
            ]
        ];
        $path = $path . "/{id}";

        $this->addPath($method, $path, $tag, $summary, $parameters);
    }

    private function genStore($resource, $path, $tag, $fields)
    {
        $method = 'post';
        $summary = "创建{$resource}";

        $parameters = collect($fields)->filter(function($field){
            return !in_array($field['field'], ['id', 'created_at', 'updated_at']);
        })->map(function($field){
            return [
                'type' => $field['type'],
                'name' => $field['field'],
                'description' => $field['comment'],
                'in' => 'query'
            ];
        })->values();

        $this->addPath($method, $path, $tag, $summary, $parameters);
    }

    private function genUpdate($resource, $path, $tag, $fields)
    {
        $method = 'put';
        $summary = "更新{$resource}";

        $parameters = collect($fields)->filter(function($field){
            return !in_array($field['field'], ['id', 'created_at', 'updated_at']);
        })->map(function($field){
            return [
                'type' => $field['type'],
                'name' => $field['field'],
                'description' => $field['comment'],
                'in' => 'query'
            ];
        })->values()->toArray();

        $parameters[] =  [
            'name' => 'id',
            'type' => 'integer',
            'in' => 'path'
        ];
        $path = $path . "/{id}";

        $this->addPath($method, $path, $tag, $summary, $parameters);
    }

    private function genDelete($resource, $path, $tag)
    {
        $method = 'delete';
        $summary = "删除{$resource}";
        $path = $path . "/{id}";
        $parameters =
            [
                [
            'name' => 'id',
            'type' => 'integer',
            'in' => 'path'
                ]
            ];

        $this->addPath($method, $path, $tag, $summary, $parameters);
    }

    public function addPath($method, $path, $tag, $summary, $parameters)
    {
        $config = $this->getSlaggerConfig();

        $template = $this->getPathTemplate($tag);
        // 接口说明
        $template['summary'] = $summary;

        // 接口参数
        $template['parameters'] = $parameters;

        $p = [
            $path => [
                $method => $template
            ]
        ];

        if(isset($config['paths'][$path])){
            $config['paths'][$path] = array_merge($config['paths'][$path], $p[$path]);
        } else {
            $config['paths'][$path] = $p[$path];
        }

        $this->writeSlaggerConfig($config);

    }
}
