<?php
/**
 * User: hitman
 * Date: 17/10/2017
 * Time: 2:49 PM
 */

namespace Hitman\Slagger\Commands;

use Illuminate\Console\Command;


class SlaggerGenDefinitionCommand extends Command
{
    use CommandTrait;

    protected $signature = 'slagger:model';
    protected $description = '创建 slagger json 中definition数据';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $modelMap = $this->getModelMap();

        $definitions = [];
        foreach($modelMap as $name => $fields){
            $definitions[$name]['type'] = 'object';
            $definitions[$name]['properties'] = collect($fields)->flatMap(function($f){
                return [$f['field'] => [ 'type' => $f['type'], 'description' => $f['comment'] ]];
            });
        }

        $config = $this->getSlaggerConfig();
        $config['definitions'] = $definitions;

        $this->writeSlaggerConfig($config);
    }
}